﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHealth : MonoBehaviour
{
    [SerializeField] private Health health;

    private void Start()
    {
        health.SetSize(.4f);
    }
}
