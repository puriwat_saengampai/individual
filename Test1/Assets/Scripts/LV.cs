﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LV : MonoBehaviour
{
    public Button easy;
    public Button hard;

    public void Start()
    {
        easy.onClick.AddListener(oneasybuttonclick);
        hard.onClick.AddListener(onhardbuttonclick);
    }

    private void oneasybuttonclick()

    {
        SceneManager.LoadScene("Game");
    }

    private void onhardbuttonclick()
    {
        SceneManager.LoadScene("Gamehard");
    }
}
