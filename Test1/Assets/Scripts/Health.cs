﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    private Transform bar;
    private void Start()
    {
        Transform bar = transform.Find("bar");
        bar.localScale = new Vector3(.4f, 1f);
    }

    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);
    }
}
