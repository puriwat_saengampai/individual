using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        public int currentHealth;
            public HealthBar healthBar;

        private void Start()
        {
                currentHealth = Hp;
                healthBar.SetMaxHealth(currentHealth);
        }

        public void Fire2()
        
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            
            var bullet1 = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet1)
            {
                bullet1.transform.position = gunPosition.position;
                bullet1.transform.rotation = Quaternion.identity;
                bullet1.SetActive(true);
                bullet1.GetComponent<Bullet>().Init(Vector2.up);                
            }            
            var bullet2 = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet2)
            {
                bullet2.transform.position = gunPosition.position;
                bullet2.transform.rotation = Quaternion.identity;
                bullet2.SetActive(true);
                bullet2.GetComponent<Bullet>().Init(Vector2.down);                
            }            
            var bullet3 = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet3)
            {
                bullet3.transform.position = gunPosition.position;
                bullet3.transform.rotation = Quaternion.identity;
                bullet3.SetActive(true);
                bullet3.GetComponent<Bullet>().Init(Vector2.left);                
            }            
            var bullet4 = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet4)
            {
                bullet4.transform.position = gunPosition.position;
                bullet4.transform.rotation = Quaternion.identity;
                bullet4.SetActive(true);
                bullet4.GetComponent<Bullet>().Init(Vector2.right);                
            }            
        }
        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            healthBar = GameManager.Instance.healthBar;
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            //SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            //var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            var bullet1 = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet1)
            {
                bullet1.transform.position = gunPosition.position;
                bullet1.transform.rotation = Quaternion.identity;
                bullet1.SetActive(true);
                bullet1.GetComponent<Bullet>().Init(Vector2.up);                
            }            
                
                       
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            object healthBer;
            healthBar.SetHealth(Hp);
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            //SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerExplode);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExplode);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}