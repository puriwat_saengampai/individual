﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace Test
{
    public class TestButton : MonoBehaviour
    
    {
        public Button Back;
        public Button GameLV;
        public void Start()
        {
            Back.onClick.AddListener(onBackbuttonclick);
            GameLV.onClick.AddListener(onGameLVbuttonclick);
        }
        public void onBackbuttonclick()
        {
            SceneManager.LoadScene("Game");
        }

        public void onGameLVbuttonclick()
        {
            SceneManager.LoadScene("GameLV");
        }
    }
}
